
use Set::Crontab;
use Test::More;

subtest 'POD example' => sub {
    my $s = Set::Crontab->new("1-9/3,>15,>30,!23", [0..30]);

    #if ($s->contains(3)) { ... }

    my @list = $s->list;
    isnt(scalar(@list), 0, 'Got non-empty list');

    foreach my $i ( @list ) {
        diag("i => $i");
    }

};

subtest 'example 2' => sub {
    my $s = Set::Crontab->new("*/5", [0..59]);

    my @list = $s->list;
    isnt(scalar(@list), 0, 'Got non-empty list');

    foreach my $i ( @list ) {
        diag("i => $i");
    }

};

done_testing;

