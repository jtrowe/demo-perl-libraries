package Demo;

use strict;
use warnings;

use Moose;

#use Data::Dump qw();
use Data::Dumper;
#use Data::Dumper::Compact qw( ddc );
use Data::Printer;
use Log::Any qw();
use Person;


has log => (
    is      => 'ro',
    builder => '_build_log',
    lazy    => 1,
);


sub _build_log {
    my $self = shift;

    $self->_init_logging;

    return Log::Any->get_logger;
}


sub _init_logging {
}


sub dump {
    my $self = shift;
    my $data = shift;

    #return Dumper($data);
    #return Data::Dump::dump($data);
    return np($data, multiline => 1);
    #return ddc($data);
}


sub run {
    my $self = shift;

    $self->log->trace('ENTERING method');

    print "self     = " .     $self  . "\n";
    print "self.ref = " . ref($self) . "\n";

    $self->log->info('Doing some work');
    sleep 2;

    my $data = {
        foo => 'the quick brown fox',
        bar => [
            'alfa', 'bravo',
        ],
        baz => {
            z => 26,
        },
        person => Person->new(name => 'Alice'),
    };

    $self->log->debug("Use Log::Any to show structured data:", $data);

    $self->log->debugf('Use caller to show structured data: %s',
            $self->dump($data));

    $self->log->info('Going to call expensive logging as TRACE');
    $self->log->trace(
        $self->expensive_to_gather_log_message,
    ) if $self->log->is_trace;


    my $anon = sub {
        my $l = Log::Any->get_logger;
        $l->info('Hello from inside anonymouse code block');
    };
    $anon->();

    my $l2 = Log::Any->get_logger(category => 'very.specific.category');
    $l2->info('Hello from a very specific category');

    my $l3 = Log::Any->get_logger(category => 'Z::Y::X::T');
    $l3->info('Hello from a different very specific category');

    my $l4 = Log::Any->get_logger(category => 'Z::Y:X::T');
    $l4->info('Hello from a different weird very specific category');

    my $l5 = Log::Any->get_logger(category => 'Z::Y.X::T');
    $l5->info('Hello from a different weird very specific category');

    $self->log->trace('EXITING method')
}


sub expensive_to_gather_log_message {
    my $self = shift;

    sleep 5;

    return 'expensive_to_gather_log_message';
}


1;
