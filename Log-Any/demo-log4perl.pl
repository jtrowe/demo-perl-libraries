use strict;
use warnings;

use lib 'lib';

use Demo;
use Log::Any;
use Log::Any::Adapter;
use Log::Log4perl;

my $log_config = q|
log4perl.rootLogger             = DEBUG, File, Screen

log4perl.appender.File          = Log::Log4perl::Appender::File
log4perl.appender.File.filename = log4perl.log
log4perl.appender.File.mode     = clobber
log4perl.appender.File.layout   = Log::Log4perl::Layout::PatternLayout
log4perl.appender.File.layout.ConversionPattern = %d [%c] mdc.foo=%X{foo} %m%n

log4perl.appender.Screen         = Log::Log4perl::Appender::Screen
log4perl.appender.Screen.layout  = Log::Log4perl::Layout::PatternLayout
log4perl.appender.Screen.layout.ConversionPattern = %p [%c] %m %n
|;
Log::Log4perl->init(\$log_config);

Log::Any::Adapter->set('Log4perl');

Log::Any->get_logger->debug('Logging initialized');

my $demo = Demo->new;
$demo->run;

my $l4p_log = Log::Log4perl::get_logger(__PACKAGE__);
$l4p_log->info('log4perl direct: Going to call expensive logging as TRACE');
$l4p_log->trace(sub {
    return $demo->expensive_to_gather_log_message;
});


