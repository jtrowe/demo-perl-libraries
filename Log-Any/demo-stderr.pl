use strict;
use warnings;

use lib 'lib';

use Demo;
use Log::Any::Adapter ( 'Stderr', log_level => 'debug' );

Demo->new->run;

